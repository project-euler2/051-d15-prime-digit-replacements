import itertools as it

def get_list_index_digit_to_change(len_changes):
    str_changes = '11' + '0'*(len_changes-2)
    options = list(dict.fromkeys(list(it.permutations(str_changes,len_changes))))

    list_of_variation = []
    for option in options:
        one_variation = []
        for i in range(0,len(option)):
            if option[i] == '1':
                one_variation.append(i)
        list_of_variation.append(one_variation)

    return list_of_variation