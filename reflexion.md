---
By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.

Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
---

One of the first thing we can notice is that a prime number needs to end by either 1, like 21 ;  3, like the examples above; 7, like 27 or 9 like 29.

We cannot change the last number because it couldn't give us 8 different prime numbers.

Also we know that the number we are searching is bigger than 56003

Let's take a 5 digit number like 65432 (let's imagine it was prime)
Maybe it's possible to have a function to which we give the number of digits we are allowed to change (here it would be 4 digits) and the function give us a list of list with for all the options :
%%432,6%%32,65%%2,%5%32,%54%2,6%3%2 --> [[0,1], [1,2],[2,3],[0,2],[0,3],[1,3]].
If we have a list like this it we can try each number. I'm not sure if it would be fast enough but it could work.

So I create a function that works like the one above : we gave it as input the len that we are able to change (for example 4 in a 5 digit numbers) and it gives us a list of indexes that we are allowed to change. My main issue is that it take 2ms to run. This might sound like not that much but it can add up if you run it a lot of times. But let's keep it for now and try to use it.

So I tried all the primes below 2 million and I didn't find any solution. What I think is that the number we are searching needs to have 3 digits replaced to work. Let's try that