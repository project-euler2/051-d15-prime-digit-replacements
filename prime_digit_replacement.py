import math
import time
import itertools as it
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(a,n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(a,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def get_list_index_digit_to_change(len_changes):
    str_changes = '111' + '0'*(len_changes-3)
    options = list(dict.fromkeys(list(it.permutations(str_changes,len_changes))))
    list_of_variation = []
    for option in options:
        one_variation = []
        for i in range(0,len(option)):
            if option[i] == '1':
                one_variation.append(i)
        list_of_variation.append(one_variation)

    return list_of_variation

def find_smallest_prime_8_replacements(min,max):
	primes = Prime_Number_In_Range(min,max)
	list_variation = get_list_index_digit_to_change(len(str(max)) - 1)
	print(list_variation)

	for prime in primes:
		for option in list_variation:
			number_of_primes = 0
			solutions = []
			for i in range(0,10):
				number_to_test = str(prime)[:option[0]] + str(i) + str(prime)[option[0] + 1:]
				number_to_test = number_to_test[:option[1]] + str(i) + number_to_test[option[1] +1:]
				number_to_test = number_to_test[:option[2]] + str(i) + number_to_test[option[2] + 1:]
				if is_prime(int(number_to_test)) and number_to_test[0] != '0':
					solutions.append(int(number_to_test))
					number_of_primes += 1
			if number_of_primes >= 8:
				return solutions

print(find_smallest_prime_8_replacements(100000,999999))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )